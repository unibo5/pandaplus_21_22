#include "../h/pcb.h"
#include "../h/listx.h"
#include "../h/pandos_const.h"
#include "../h/pandos_types.h"
#include <umps3/umps/const.h>

#define TRUE 1
#define FALSE 0


static struct list_head _pcbFree_h=LIST_HEAD_INIT(_pcbFree_h);
static struct list_head* pcbFree_h = &_pcbFree_h;
static pcb_t pcbFree_table[MAXPROC];


void initPcbs(){ 
    for(int k = 0;  k < MAXPROC; k++){
    list_add(&pcbFree_table[k].p_list,pcbFree_h);    
  }    
}

void freePcb(pcb_t * p){ 
  if(p != NULL){
    list_add(&p->p_list,pcbFree_h);
  }
}

pcb_t * setNullPcb(pcb_t * p){    
    p->p_list.next = &p->p_list;
	p->p_list.prev = &p->p_list;
	p->p_parent=NULL;
	p->p_child.prev=&p->p_child;
	p->p_child.next=&p->p_child;
	p->p_sib.next=&p->p_sib;
	p->p_sib.prev=&p->p_sib;
	p->p_time=0;
	p->p_s.cause=0;
	p->p_s.entry_hi=0;	
	p->p_s.hi=0;
	p->p_s.lo=0;
	p->p_s.pc_epc=0;
	p->p_s.status=0;
	p->p_semAdd=NULL;	
	for(int i=0;i<STATE_GPR_LEN;i++){
		p->p_s.gpr[i]=0;
	}
	return p;
}     

pcb_t *allocPcb(){
    if(list_empty(pcbFree_h)) return NULL; 
	struct list_head entry;
	pcb_t * p= container_of(pcbFree_h->next,pcb_t,p_list);	
    list_del(pcbFree_h->next);	  
    return setNullPcb(p); 
}        

void mkEmptyProcQ(struct list_head *head) {
	INIT_LIST_HEAD(head);
}

int emptyProcQ(struct list_head *head) {
	return list_empty(head);
}

void insertProcQ(struct list_head *head, pcb_t *p) {
	list_add_tail(&p->p_list, head);
}

pcb_t *headProcQ(struct list_head *head) {
	if(list_empty(head)) return NULL;
	return container_of(list_next(head), pcb_t, p_list);
}

// Lista dei PCB
pcb_t *removeProcQ(struct list_head *head) {
	if (list_empty(head)) return NULL;
		pcb_t * p= headProcQ(head);
		list_del(&p->p_list);
		return p;	
}


pcb_t *outProcQ(struct list_head *head, pcb_t *p) {
	struct list_head *tmp = list_next(head);
	
	while(head!=tmp) {
		if(p == container_of(tmp, pcb_t, p_list)) {
			list_del(tmp);
			return p;
		} else {
			tmp = list_next(tmp);
		}
	}
	return NULL;
}

int emptyChild(pcb_t *p) {
	return list_empty(&p->p_child);	
}

void insertChild(pcb_t *prnt, pcb_t *p) {
	insertProcQ(&prnt->p_child, p);
	p->p_parent=prnt;
}

pcb_t *removeChild(pcb_t *p) {
	if(!emptyChild(p)){
		struct list_head *tmp=list_next(&p->p_child);
		list_del(tmp);
		return container_of(tmp, pcb_t, p_list);
	}
	return NULL;
}

int haveAFather(pcb_t *p){
	if(p->p_parent==NULL)return 0;
	else
	 return 1;;
}

pcb_t *outChild(pcb_t *p) {
	if(haveAFather(p)){
		pcb_t* tmp=outProcQ(&p->p_parent->p_child, p);
		tmp->p_parent=NULL;
		return tmp;
	}
	else return NULL;	
}






































