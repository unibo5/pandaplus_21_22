#include <umps3/umps/aout.h>
#include <umps3/umps/arch.h>
#include <umps3/umps/libumps.h>
#include <pandos_libs.h>
#include <../resources/pandos_const.h>
#include <../resources/pandos_types.h>
#include <../phase1/pcb.h>
#include <../phase1/asl.h>
#include <scheduler.h>
#include <exceptionHandler.h>
#include <interrupts.h>

/* Macro to load the Interval Timer  p.149*/
#define LDIT(T)
	((* ((cpu_t *) INTERVALTMR)) = (T) * (* ((cpu_t *) TIMESCALEADDR)))

// started but not terminated processes
int processCount;

// started, but not terminated processes that are in the "blocked" state
int softBlockedCount;

// tail pointer to a queue of pcbs that are in the "ready" state
pcb_t *readyQueueLow;
pcb_t *readyQueueHigh;

// pointer to the pcb that is in the "running" state
pcb_t *currentProcess;

// semaphores for each (sub)device
int deviceSemaphores[49];

// passupvector
passupvector_t *passUpVector; // = (passupvector_t *) PASSUPVECTOR;

/* The initial processor state */
HIDDEN state_t initialState;

/* Include the test function, in order to execute p2test.c */
extern void test();

/* Placeholder function for TLB-Refill */
extern void uTLB_RefillHandler();


int main() {
    // 2)
	passUpVector = (passupvector_t *) PASSUPVECTOR;

	passUpVector->tlb_refill_handler = (memaddr) uTLB_RefillHandler;
    passUpVector->tlb_refill_stackPtr = (memaddr) KERNELSTACK;
    passUpVector->exception_handler = (memaddr) exceptionHandler;
    passUpVector->exception_stackPtr = (memaddr) KERNELSTACK;

    // 3)
    initPcbs(); 
    initASL();

    // 4)
    softBlockCount = 0;
    processCount = 0;

    readyQueueLow = mkEmptyProcQ();

    currentProcess = NULL;

    for (int i = 0; i < 49; ++i) {
    	deviceSemaphores[i] = 0;
    }

    // 5) load the system-wide Interval Timer with 100 milliseconds
    LDIT(PSECOND);

    // 6) istantiate a single low priority process, place its pcb in the ready queue, and inc pCount
    pcb_PTR process = allocPCB();
    process->p_prio = 0;

    processCount++;

    process->p_time = 0;
    process->p_semAdd = NULL;
    process->p_supportStruct = NULL;

    /* setting up the first process state */
	STST(&initialState);	/* create a state area */	

    /* SETTING SP TO RAMTOP */
    RAMTOP(initialState.reg_sp);

    /*inizializzazione pc all'indirizzo della funzione test*/
    initialState.pc_epc = (memaddr) test;
    initialState.reg_t9 = (memaddr) test;

    /*Attiviamo interrupt, timer e kernel mode*/
    initialState.status = IEPON | IMON | TEBITON;

    currentProcess = NULL;
    if(process->p_prio == 0) {
        insertProcQ(&readyQueueLow, process);
    } else {
        insertProcQ(&readyQueueHigh, process);
    }

    /*Chiamo lo Scheduler*/
    scheduler();

    return 0;
}