#include <scheduler.h>
#include <pcb.h>
#include <../resources/pandos_const.h>
#include <../resources/pandos_types.h>

extern int processCount;
extern int softBlockedCount;
extern pcb_t readyQueueLow;
extern pcb_t readyQueueHigh;
extern pcb_t currentProcess;
extern pcb_t pcbFree_h;
extern int deviceSemaphores[49];
extern cpu_t startTimer;
extern cpu_t endTimer;

int processPid = 0;

void syscallExceptionHandler(state_t *execState) {
	/*Incremento PC*/
    execState->s_pc += WORDLEN;

	if((exception_state->status & USERPON) == ALLOFF) {
		syscall(execState);
	} else {
		// pagina 16 del manuale, paragrafo 3.5.11
		
		execState->cause = execState->cause & 0x3;	//3  = 000011
		execState->cause = execState->cause | 0x28;	//28 = 101000
		//passUpOrDie(GENERALEXCEPT, execState);
		
		exceptionHandler();
	}
}

void syscall(state_t *execState) {

	switch(execState->s_a0) {
		case CREATEPROCESS:
			SYS1(execState);
			break;
		case TERMPROCESS:
			SYS2(execState);
			break;
		case PASSEREN:
			passeren_SYS3(execState);
			break;
		case VERHOGEN:

			break;
		case DOIO:

			break;
		case GETTIME:

			break;
		case CLOCKWAIT:

			break;
		case GETSUPPORTPTR:

			break;
		case GETPROCESSID:

			break;
		case YIELD:

			break;
		default:
			passUpOrDie(GENERALEXCEPT, execState);
			break;
	}

}

// create process
void SYS1(state_t *execState) {
	if(emptyProcQ(pcbFree_h) == 0) {
		execState->s_v0 = NOPROC;	// return -1 in the caller's v0
		LDST(execState);
	}

	//pcb_t *newProc = container_of(&currentProcess->p_child, pcb_t, p_child);

	pcb_t *newProc = allocPCB();
	newProc->p_s = (state_t*) execState->s_a1;
	newProc->p_prio = execState->s_a2;
	newProc->p_supportStruct = (support_t*) execState->s_a3;
	newProc->p_pid = processPid++;

	if(newPro->p_prio == 0) {
		insertProcQ(readyQueueLow->p_list, newProc);
	} else {
		insertProcQ(readyQueueHigh->p_list, newProc);
	}

	insertChild(currentProcess, newProc);
	newProc->p_time = 0;
	newProc->p_semAdd = NULL;

	processCount++;

	execState->s_v0 = newProc->p_pid;
	LDST(execState);
}

pcb_t *removeProgeny(pcb_t *pcb) {
	pcb_t *removed = removeChild(pcb);
	removed == NULL ? NULL : removeProgeny(container_of(removed->p_child, pcb_t, p_child));
}

// terminate process
void SYS2(state_t *execState) {
	outChild(currentProcess);
	removeProgeny(currentProcess);
	// valutare se rimuovere currentProcess anche da pcbFree_h

	freePcb(currentProcess);
	processCount--;

	scheduler();
}

// semaphore request (PASSEREN)
void passeren_SYS3(state_t *execState) {

	copyState(execState, currentProcess->p_s);

	int *semAddr = (int*) execState->s_a1;
	(*semAddr)--;

	// the process has to be blocked
	if(*semAddr < 0) {
		currentProcess->p_time = getCpuTime();
		insertBlocked(semAddr, currentProcess);
		scheduler();
	} else {
		LDST(execState);
	}
}

// free semaphore
void verhogen_SYS4(state_t *execState) {
	int *semAddr = (int*) execState->s_a1;
	(*semAddr)++;

	pcb_t removedProcess = removeBlocked(semAddr);

	if(removedProcess != NULL) {
		removedProcess->p_semAdd = NULL;
		if(removedProcess->p_prio == 0) {
			insertProcQ(readyQueueLow->p_list, removedProcess);
		} else {
			insertProcQ(readyQueueHigh->p_list, removedProcess);
		}

	}
	LDST(execState);
}

// calcolo indice semaforo (cercare fonti)
int getDeviceSemaphoreIndex(int line, int device, int read) {
    return ((line - 3) * 8) + (line == 7 ? (read * 8) + device : device);
}

// effettua un'operazione di I/O
void do_io_SYS5(state_t *execState) {
	copyState(execState, currentProcess->p_s);

	int line = execState->s_a1;
	int dev = execState->s_a2;
	int rorT = execState->s_a3;
	int index = getDeviceSemaphoreIndex(line, dev, rorT);	// calcolo l'indice del semaforo
	int *sem = &(deviceSemaphores[index]);	// prendo l'indirizzo del semaforo

	currentProcess->p_time = getCpuTime();

	(*sem)--;
	// process is softblocked
	insertBlocked(sem, currentProcess);
	softBlockedCount++;

	scheduler();
}

// restituisce il tempo di esecuzione del processo chiamante fino a quel momento
void get_cpu_time_SYS6(state_t *execState) {
	cpu_t currTime;
	currentProcess->p_time += (STCK(currTime) - startTimer);

	// salvo il tempo di esecuzione in v0
	execState->s_v0 = currentProcess->p_time;

	STCK(startTimer);
	LDST(execState);
}

// blocca il processo invocante fino al prossimo tick del dispositivo (DA RIVEDERE)
void wait_for_clock_SYS7(state_t *execState) {
	copyState(execState, currentProcess->p_s);
	currentProcess->p_time = getCpuTime();
	softBlockedCount++;

	(deviceSemaphores[49])--;
	// blocco il processo e chiamo lo scheduler
	insertBlocked(&deviceSemaphores[49], currentProcess);
	scheduler();
}

// restituisce un puntatore alla struttura di supporto del processo corrente
void get_support_data_SYS8(state_t *execState) {
	execState->s_v0 = (unsigned int) currentProcess->p_supportStruct;

	LDST(execState);
}

// restituisce l'identificatore del processo invocante (parent == 0), del genitore altrimenti
void get_process_id_SYS9(state_t *execState) {
	if(currentProcess->p_parent->p_pid == 0) {
		execState->s_v0 = currentProcess->p_pid;
	} else {
		execState->s_v0 = currentProcess->p_parent->p_pid;
	}

	LDST(execState);
}

// il processo chiamante viene sospeso e messo in fondo alla coda dei proessi ready
void yeld_SYS10(state_t *execState) {
	pcb_t tmp;
	pcb_t *readyQueue;
	if(currentProcess->p_prio == 0) {
		tmp = removeProcQ(readyQueueLow, currentProcess);
		readyQueue = readyQueueLow;
	} else {
		tmp = removeProcQ(readyQueueHigh, currentProcess);
		readyQueue = readyQueueHigh;
	}
	
	pcb_t tmp1;
	pcb_t tmp2;
	do {
		tmp2 = tmp1;
		tmp1 = headProcQ(readyQueue);
	} while(tmp1 != NULL);

	insertProcQ(tmp2->p_list, currentProcess);

	//LDST(execState);
}

void copyState(state_t * src, state_t * dest){

	dest->s_entryHI = src->s_entryHI;
	dest->s_cause = src->s_cause;
	dest->s_status = src->s_status;
	dest->s_pc = src->s_pc;
	for(int i = 0; i < 31; i++) dest->s_reg[i] = src->s_reg[i];
}

cpu_t getCpuTime(){
	return currentProcess->p_time + (endTimer - startTimer);
}