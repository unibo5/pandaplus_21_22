#include <scheduler.h>
#include <pcb.h>
#include <../resources/pandos_const.h>
#include <../resources/pandos_types.h>

extern pcb_t currentProcess;

void passUpOrDie(unsigned int cont, state_t *execState) {
	if(currentProcess->p_supportStruct == NULL) {
		//SYSCALL(TERMPROCESS, currentProcess->p_pid, 0, 0);
		terminateProcess();
	}
	else {
		/*copia lo stato del processo che ha causato l'eccezione nella support struct*/
        copyState(execState, &(currentProcess->p_supportStruct->sup_exceptState[cont]));

        /*salva lo stack pointer, status e pc del context corretto*/
        unsigned int stackPtr = currentProcess->p_supportStruct->sup_exceptContext[cont].c_stackPtr;
        unsigned int status = currentProcess->p_supportStruct->sup_exceptContext[cont].c_status;
        unsigned int pc = currentProcess->p_supportStruct->sup_exceptContext[cont].c_pc;

        /*carica il context per gestire l'eccezione*/
        LDCXT(stackPtr, status, pc);	// serve la kernel-mode, altrimenti break-point exception
	}
}

void exceptionHandler() {
	state_t *execState = (state_t *) BIOSDATAPAGE;

	 /*
    0x3c = 11 1100, che significa che prendendo tutto il registro Cause e facendo
    un'operazione di AND, posso "isolare" i bit 2 - 6, che corrispondono a execState.
    Inoltre, con un uno shift di 2 posizioni ottengo soltanto i bit 2-6.
    (Cause register al par. 3.3 pops)
    */
	int exc_code = (execState->cause & 0x3C) >> CAUSESHIFT;

	switch(exc_code) {
		case 0:
			interruptHandler();
			break;
		case 1 ... 3:
			passUpOrDie(PGFAULTEXCEPT, execState);	//p. 23 section 3.7.2
			break;
		case 4 ... 7:
		case 9 ... 12:
			passUpOrDie(GENERALEXCEPT, execState);
			break;
		case 8:
			syscallExceptionHandler(execState);
			break;
	}

}