#include <scheduler.h>
#include <pcb.h>
#include <../resources/pandos_const.h>
#include <../resources/pandos_types.h>

extern int processCount;
extern int softBlockedCount;
extern pcb_t readyQueueLow;
extern pcb_t readyQueueHigh;
extern pcb_t currentProcess;
extern int deviceSemaphores[49];

cpu_t startTimer;
cpu_t endTimer;

void scheduler() {
	
	if(currentProcess != NULL) {
		
		STCK(endTimer);
		currentProcess->p_time += (endTimer - startTimer);
	}

	currentProcess = removeProcQ(&readyQueueHigh);
	if(currentProcess == NULL) {
		currentProcess = removeProcQ(&readyQueueLow);
	}
	
	if(currentProcess != NULL) {
		STCK(startTimer);

		// se esistono processi ad alta priorità non faccio setTimer()
		if(currentProcess->p_prio == 0) {
			setTimer(TIMESLICE);
		}

		LDST(&(currentProcess->p_s));

	} else {
		if(processCount == 0) {
			HALT();
		}
		if(processCount > 0 && softBlockedCount > 0) {
			// enable interrupts and disable the PLT
			setSTATUS(ALLOFF | IECON | IMON);

			WAIT();
		}
		if(processCount > 0 && softBlockedCount == 0) {
			// Deadlock
			PANIC();
		}


	}
	




}