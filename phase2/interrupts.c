#include <scheduler.h>
#include <pcb.h>
#include <../resources/pandos_const.h>
#include <../resources/pandos_types.h>
#include <umps3/umps/arch.h>
#include ‘‘/usr/include/umps3/umps/libumps.h’’

extern int processCount;
extern int softBlockedCount;
extern pcb_t currentProcess;
extern int deviceSemaphores[49];
extern cpu_t startTimer;
extern cpu_t endTimer;

memaddr* getDevRegAddr(int intLineNo, int devNo) {
    return (memaddr*) (0x10000054 + ((intLineNo - 3) * 0x80) + (devNo * 0x10));
}

void interruptExceptionHandler(unsigned int causeReg) {

    //get Interrupts Pending
    unsigned int IP = (causeReg && 0xFF00) >> 8;
    int bitChecker = 2;
    //Trova il primo interrupt in ordine di priorità (interrupt line 0 può essere ignorato, controllo da 1 a 7)
    for (int interruptLine = 1; interruptLine < 8; interruptLine++) {
        if (IP & bitChecker)
            interruptSwitch(interruptLine);
        bitChecker *= 2;
    }
}

void interruptSwitch(int interruptLine)
{
    switch(interruptLine)
    {
        case 1:
            //PLT
        	pltInterrupt();
            break;
        case 2:
            //Interval Timer
        	intervalTimerInterrupt();
            break;
        case 3 ... 7:
            //Non-Timer Interrupts
            memaddr* deviceAddress = (memaddr*) ( 0x10000040 + ((interruptLine - 3) * 0x04));
            int bitChecker = 2;
            for (int deviceNumber = 0; deviceNumber < DEVPERINT; deviceNumber++) //cerco il device con priorità più alta
            {                                           //DEVPERINT = devices per interrupt line (8)
                if (*deviceAddress & bitChecker)
                    nonTimer(interruptLine, deviceNumber);
                bitChecker *= 2;
            }
            break;
        default:
            PANIC();
            break;
    }
}

void nonTimer(int interruptLine, int deviceNumber)
{
    //devreg_t a pag 142 manuale lungo uMPS3
    devreg_t* deviceRegister = (devreg_t*) (memaddr*) (0x10000054 + ((interruptLine - 3) * 0x80) + (deviceNumber * 0x10));

    unsigned int statusCode = deviceRegister->dtp.status;

    deviceRegister->dtp.command = ACK;  //ACK è 1 btw

    int index = (line - 3) * 8 + deviceNumber;
    deviceSemaphores[index] += 1;
    pcb_t* unlockedProcess = removeBlocked(&deviceSemaphores[index]);

    unlockedProcess->p_s.reg_v0 = statusCode;   //dichiarato correttamente?
}

// line 1
void pltInterrupt() {
	setTIMER(9999);

	// copy old state on current process
    copyState((state_t*) BIOSDATAPAGE, &currentProcess->p_s);

	// load the PLT with the value of 5 milliseconds
    currentProcess->p_time += 5000;

    // set the current process to ready state
	if(currentProcess->p_prio == 0) {
		insertProcQ(readyQueueLow->p_list, currentProcess);
	} else {
		insertProcQ(readyQueueHigh->p_list, currentProcess);
	}

    scheduler();
}

// line 2
void intervalTimerInterrupt() {
	// Acknowledge the interrupt by loading the Interval Timer with a new value: 100 milliseconds
	LDIT(100000);

	// Unblock ALL pcbs blocked on the Pseudo-clock semaphore
	while(headBlocked(&(deviceSemaphores[49-1])) != NULL) {
        pcb_PTR unblockedProcess = removeBlocked(&deviceSemaphores[49-1]);

        // there is a process blocked
        if(unblockedProcess != NULL){
        	// increasing process time
        	unblockedProcess->p_time + (endTimer - startTimer);

        	unblockedProcess->p_semAdd = NULL;
        	
        	// set the current process to ready state
			if(unblockedProcess->p_prio == 0) {
				insertProcQ(readyQueueLow->p_list, unblockedProcess);
			} else {
				insertProcQ(readyQueueHigh->p_list, unblockedProcess);
			}
            softBlockedCount--;
        }

        deviceSemaphores[49-1] = 0;

        // if currentProcess is null, return control to the scheduler
        if(currentProcess != NULL){
        	// load old state
            LDST((state_t*) BIOSDATAPAGE);
        } else {
            scheduler();
        }
	}

}